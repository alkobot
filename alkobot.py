#!/usr/bin/env python
# coding: utf-8

from jabberbot import JabberBot, botcmd
from datetime import datetime
import time
import urllib2
import re
import random
import json
import config


class DrahyInfo(object):
    re_wg_timeleft = re.compile(r'<tr.*><td.*>(\d+) Hod a (\d+) Min</td></tr>')
    re_wg_inprogress = re.compile(r'<tr.*><td.*>.*do konce.*(\d+)\s*min\s*</td></tr>')

    def __init__(self, url, maxAge = 60):
        self.url = url
        self.maxAge = maxAge
        self.clear()

    def clear(self):
        self.stamp = 0
        self.wg_hours, self.wg_minutes, self.wg_at = None, None, None
        self.wg_left, self.wg_end = None, None

    def getInfo(self):
        stamp = time.time()
        page = urllib2.urlopen(self.url).read()
        # wintergrasp bude
        found_wg1 = self.re_wg_timeleft.search(page)
        found_wg2 = self.re_wg_inprogress.search(page)
        if found_wg1:
            self.wg_hours, self.wg_minutes = map(int, found_wg1.groups())
            #self.wg_hours, self.wg_minutes = int(self.wg_hours), int(self.wg_minutes)
            self.wg_at = stamp + self.wg_hours * 3600 + self.wg_minutes * 60
            self.stamp = stamp
            self.wg_left, self.wg_end = None, None
        # wintergrasp uz byl
        elif found_wg2:
            self.wg_left = int(found_wg2.group(1))
            self.wg_end = stamp + self.wg_left * 60
            self.stamp = stamp
        else:
            self.clear()

    def refresh(self):
        if time.time() > self.maxAge + self.stamp:
            print "refresh serverinfo"
            self.getInfo()
#endclass DrahyInfo


class PlayerInfo(object):
    def __init__(self, url, maxAge = 60):
        self.url = url
        self.maxAge = maxAge
        self.players = {}
        self.stamp = 0

    def getInfo(self):
        stamp = time.time()
        page = urllib2.urlopen(self.url).read()
        # vynechat balast na zacatku a konci [4:-3]
        data = page.split("\n")[4]
        data = data.replace("'", '"')   # json do not like apostrophes
        data = data.decode("cp1250")    # drahy claims wrong codepage
        data = json.loads(data)
        self.players = dict([(i['name'].lower(), i) for i in data['online'] if type(i) is dict ])
        self.stamp = stamp

    def refresh(self):
        if time.time() > self.maxAge + self.stamp:
            print "refresh playerinfo"
            self.getInfo()

    def search(self, name):
        self.refresh()
        #print self.players.keys()
        return self.players.get(name, {})
#endclass


class Alkobot1(JabberBot):
    def __init__(self, serverInfo, playerInfo, *args, **kargs):
        self.serverInfo = serverInfo
        self.playerInfo = playerInfo
        # initialize jabberbot
        super(Alkobot1, self).__init__(*args, **kargs)

    @botcmd
    def wg(self, mess, args):
        self.serverInfo.refresh()
        if self.serverInfo.wg_end:
            return "Bitka o wintergrasp končí v %s (za 00:%s)" % (
                time.strftime("%H:%M", time.localtime(self.serverInfo.wg_end)),
                self.serverInfo.wg_left,
            )
        elif self.serverInfo.wg_at:
            return "Hospodo, wintergrasp začíná v %s (za %s:%s)" % (
                time.strftime("%H:%M", time.localtime(self.serverInfo.wg_at)),
                self.serverInfo.wg_hours,
                self.serverInfo.wg_minutes,
            )
        else:
            return "Nemám páru"
    #enddef

    @botcmd
    def najdi(self, mess, args):
        args = args.lower().strip()
        if not args:
            return "Koho?"

        vyhybka = {
            "metlos": "Ve stadtlu",
            "ratus": "zere",
            "radon": "zere",
        }
        if args in vyhybka:
            return vyhybka[args]

        player = self.playerInfo.search(args.lower().strip())

        if player:
            return "%(name)s je v %(zone)s" % player
        else:
            return "Nenašli moji rádcové, nenašli"

    @botcmd
    def cas(self, mess, args):
        return str(datetime.now())

    @botcmd
    def foo(self, mess, args):
        print "message", type(mess)
        print mess
        print "args", type(args)
        print args

    @botcmd
    def kozy(self, mess, args):
        if random.random() < 0.1:
            return "ven!"
        return "prasaku!"

    @botcmd
    def barman(self, mess, args):
        return "jaaaaaaaaaaa"


# =======================================================
if __name__ == '__main__':

    #bot = Example(username, password, only_direct=True)
    bot = Alkobot1(
            DrahyInfo("http://drahy.com/"),
            PlayerInfo("http://drahy.com/components/pomm/pomm_play.php"),
            config.username,
            config.password)
    bot.join_room(config.chatroom, config.nickname)

    bot.serve_forever()

